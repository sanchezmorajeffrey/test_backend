<?php

namespace Database\Seeders;

use App\Models\Series;
use Illuminate\Database\Seeder;

class SeriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $this->createSeries();
    }

    public function createSeries(): void
    {
        $series = [
            array(
                'prefix' =>  "CLI-2023",
                'number' => 0,
                'is_active' => true,
            ),
            array(
                'prefix' =>  "PRE-2023",
                'number' => 0,
                'is_active' => true,
            )
        ];

        foreach ($series as $key => $value) {
            Series::create($value);
        }
    }
}
