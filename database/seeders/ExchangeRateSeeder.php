<?php

namespace Database\Seeders;

use App\Models\ExchangeRate;
use Illuminate\Database\Seeder;

class ExchangeRateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $this->createExchangeRates();
    }

    public function createExchangeRates()
    {
        $reate = [
            array(
                'year' => 2023,
                'month' => 3,
                'date' => '2023-03-01',
                'buy' => 36.3211
            ),
            array(
                'year' => 2023,
                'month' => 3,
                'date' => '2023-03-02',
                'buy' => 36.3221
            ),
            array(
                'year' => 2023,
                'month' => 3,
                'date' => '2023-03-03',
                'buy' => 36.3230
            ),
            array(
                'year' => 2023,
                'month' => 3,
                'date' => '2023-03-04',
                'buy' => 36.3240
            ),
            array(
                'year' => 2023,
                'month' => 3,
                'date' => '2023-03-05',
                'buy' => 36.3250
            ),
            array(
                'year' => 2023,
                'month' => 3,
                'date' => '2023-03-06',
                'buy' => 36.3260
            ),
            array(
                'year' => 2023,
                'month' => 3,
                'date' => '2023-03-07',
                'buy' => 36.3270
            ),
            array(
                'year' => 2023,
                'month' => 3,
                'date' => '2023-03-08',
                'buy' => 36.3280
            ),
            array(
                'year' => 2023,
                'month' => 3,
                'date' => '2023-03-09',
                'buy' => 36.3290
            ),
            array(
                'year' => 2023,
                'month' => 3,
                'date' => '2023-03-10',
                'buy' => 36.3300
            ),
            array(
                'year' => 2023,
                'month' => 3,
                'date' => '2023-03-11',
                'buy' => 36.3310
            ),
            array(
                'year' => 2023,
                'month' => 3,
                'date' => '2023-03-12',
                'buy' => 36.3320
            ),
            array(
                'year' => 2023,
                'month' => 3,
                'date' => '2023-03-13',
                'buy' => 36.3329
            ),
            array(
                'year' => 2023,
                'month' => 3,
                'date' => '2023-03-14',
                'buy' => 36.3339
            ),
            array(
                'year' => 2023,
                'month' => 3,
                'date' => '2023-03-15',
                'buy' => 36.3349
            ),
            array(
                'year' => 2023,
                'month' => 3,
                'date' => '2023-03-16',
                'buy' => 36.3359
            ),
            array(
                'year' => 2023,
                'month' => 3,
                'date' => '2023-03-17',
                'buy' => 36.3369
            ),
            array(
                'year' => 2023,
                'month' => 3,
                'date' => '2023-03-18',
                'buy' => 36.3379
            ),
            array(
                'year' => 2023,
                'month' => 3,
                'date' => '2023-03-19',
                'buy' => 36.3389
            ),
            array(
                'year' => 2023,
                'month' => 3,
                'date' => '2023-03-20',
                'buy' => 36.3399
            ),
            array(
                'year' => 2023,
                'month' => 3,
                'date' => '2023-03-21',
                'buy' => 36.3409
            ),
            array(
                'year' => 2023,
                'month' => 3,
                'date' => '2023-03-22',
                'buy' => 36.3419
            ),
            array(
                'year' => 2023,
                'month' => 3,
                'date' => '2023-03-23',
                'buy' => 36.3429
            ),
            array(
                'year' => 2023,
                'month' => 3,
                'date' => '2023-03-24',
                'buy' => 36.3438
            ),
            array(
                'year' => 2023,
                'month' => 3,
                'date' => '2023-03-25',
                'buy' => 36.3448
            ),
            array(
                'year' => 2023,
                'month' => 3,
                'date' => '2023-03-26',
                'buy' => 36.3458
            ),
            array(
                'year' => 2023,
                'month' => 3,
                'date' => '2023-03-27',
                'buy' => 36.3468
            ),
            array(
                'year' => 2023,
                'month' => 3,
                'date' => '2023-03-28',
                'buy' => 36.3478
            ),
            array(
                'year' => 2023,
                'month' => 3,
                'date' => '2023-03-29',
                'buy' => 36.3488
            ),
            array(
                'year' => 2023,
                'month' => 3,
                'date' => '2023-03-30',
                'buy' => 36.3498
            ),
            array(
                'year' => 2023,
                'month' => 3,
                'date' => '2023-03-31',
                'buy' => 36.3508
            ),
        ];

        foreach ($reate as $key => $value) {
            ExchangeRate::create($value);
        }
    }
}
