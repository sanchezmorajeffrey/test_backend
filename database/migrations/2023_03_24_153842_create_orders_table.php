<?php

use App\Enum\OrderCurrency;
use App\Enum\OrderStatus;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->decimal('subtotal', 15, 2);
            $table->decimal('taxes', 15, 2);
            $table->decimal('total', 15, 2);
            $table->char('currency', 3)->default(OrderCurrency::NIO->value);
            $table->string('code');
            $table->unsignedTinyInteger('status')->default(OrderStatus::Completed->value);
            $table->foreignId('client_id')->constrained('clients')->onDelete('cascade');
            $table->foreignId('series_id')->constrained('series')->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('orders');
    }
};
