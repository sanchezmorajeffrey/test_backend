<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Product extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'name',
        'description',
        'is_active',
        'price',
        'sku'
    ];

    protected $casts = [
        'is_active' => 'boolean',
        'price' => 'float'
    ];

    public function scopeActiveProduct($query)
    {
        return $query->where('is_active', true);
    }
}
