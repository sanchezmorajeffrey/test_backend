<?php

namespace App\Models;

use App\Enum\OrderStatus;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Order extends Model
{
    use HasFactory;

    protected $fillable = ['taxes', 'subtotal', 'total', 'client_id', 'status', 'taxes', 'currency', 'code', 'series_id'];

    protected $casts = [
        'taxes' => 'float',
        'subtotal' => 'float',
        'total' => 'float',
        'status' => OrderStatus::class,
    ];

    public function client(): BelongsTo
    {
        return $this->belongsTo(Client::class);
    }

    public function items(): HasMany
    {
        return $this->hasMany(OrderItem::class);
    }

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $series = Series::where('id', $model->series_id)->first();
            $series_count = Series::where('id', $model->series_id)->max('number') + 1;
            $model->code = $series->prefix . '-' . str_pad($series_count, 5, '0', STR_PAD_LEFT);
            $series->number = $series->number + 1;
            $series->save();
        });
    }
}
