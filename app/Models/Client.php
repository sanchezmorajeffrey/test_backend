<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Client extends Model
{
    use HasFactory;
    use SoftDeletes;

    public $table = 'clients';

    protected $fillable = [
        'code',
        'first_name',
        'last_name',
        'email',
        'phone',
        'address',
        'series_id'
    ];

    public function series(): BelongsTo
    {
        return $this->belongsTo(Series::class, 'series_id');
    }

    public function orders(): HasMany
    {
        return $this->hasMany(Order::class);
    }

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $series = Series::where('id', $model->series_id)->first();
            $series_count = Series::where('id', $model->series_id)->max('number') + 1;
            $model->code = $series->prefix . '-' . str_pad($series_count, 5, '0', STR_PAD_LEFT);
            $series->number = $series->number + 1;
            $series->save();
        });
    }
}
