<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ExchangeRate extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'date',
        'buy',
        'year',
        'month'
    ];

    protected $casts = [
        'buy' => 'float',
    ];
}
