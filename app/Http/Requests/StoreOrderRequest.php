<?php

namespace App\Http\Requests;

use App\Enum\OrderStatus;
use App\Enum\OrderCurrency;
use Illuminate\Validation\Rules\Enum;
use Illuminate\Foundation\Http\FormRequest;

class StoreOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'client_id' => 'required|integer|exists:clients,id',
            'status' => ['required', new Enum(OrderStatus::class)],
            'currency' => ['required', new Enum(OrderCurrency::class)],
            'products' => 'present|array|min:1',
            'products.*.product_id' => 'required|integer|exists:products,id',
            'products.*.quantity' => 'required|numeric|min:1',
            'products.*.price' => 'required|numeric',
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'status' => OrderStatus::Completed,
            'series_id' => 2
        ]);
    }
}
