<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ExchangeRatePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'rate' => 'present|array|min:1',
            'rate.*.date' => 'required|date_format:Y-m-d|unique:exchange_rates',
            'rate.*.buy' => 'required|numeric',
            'rate.*.year' => 'date_format:Y',
            'rate.*.month' => 'numeric|between:1,12',
        ];
    }
}
