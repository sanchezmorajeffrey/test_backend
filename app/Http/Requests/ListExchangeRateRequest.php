<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ListExchangeRateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'month' => 'required|numeric|between:1,12',
            'year' => 'required|date_format:Y'
        ];
    }
}
