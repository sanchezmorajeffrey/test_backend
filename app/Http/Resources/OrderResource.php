<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'subtotal' => $this->subtotal,
            'taxes' => $this->taxes,
            'total' => $this->total,
            'currency' => $this->currency,
            'code' => $this->code,
            'status' =>  $this->status->name,
            'items' => ItemResource::collection($this->whenLoaded('items')),
            'client' =>  new ClientResource($this->whenLoaded('client')),
            'created_at' => $this->created_at
        ];
    }
}
