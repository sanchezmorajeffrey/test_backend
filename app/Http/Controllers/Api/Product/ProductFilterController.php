<?php

namespace App\Http\Controllers\Api\Product;

use App\Http\Controllers\Controller;
use App\Services\Product\ProductService;
use App\Http\Requests\ListProductByFilterRequest;

class ProductFilterController extends Controller
{
    public function __construct(private ProductService $productService)
    {
    }
    /**
     * Handle the incoming request.
     */
    public function __invoke(ListProductByFilterRequest $request)
    {
        return $this->productService->filterSkuDescription($request);
    }
}
