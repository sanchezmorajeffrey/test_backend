<?php

namespace App\Http\Controllers\Api\ExchangeRate;

use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Http\Requests\DeleteExchangetRequest;
use App\Services\ExchangeRate\ExchangeRate as ExchangeRateExchangeRate;

class ExchangeRateDeleteController extends Controller
{
    public function __construct(private ExchangeRateExchangeRate $rateService)
    {
    }
    /**
     * Handle the incoming request.
     */
    public function __invoke(DeleteExchangetRequest $request): Response
    {
        return $this->rateService->destroy($request);
    }
}
