<?php

namespace App\Http\Controllers\Api\ExchangeRate;

use App\Http\Controllers\Controller;
use App\Http\Requests\ListExchangeRateRequest;
use App\Services\ExchangeRate\ExchangeRate as ExchangeRateExchangeRate;

class ExchangeRateFilterMonthController extends Controller
{
    public function __construct(private ExchangeRateExchangeRate $rateService)
    {
    }
    /**
     * Handle the incoming request.
     */
    public function __invoke(ListExchangeRateRequest $request)
    {
        return $this->rateService->filterByMonth($request);
    }
}
