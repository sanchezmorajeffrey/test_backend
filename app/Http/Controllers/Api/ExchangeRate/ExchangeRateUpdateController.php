<?php

namespace App\Http\Controllers\Api\ExchangeRate;

use App\Http\Controllers\Controller;
use App\Http\Requests\ExchangeRateUpdateRequest;
use App\Services\ExchangeRate\ExchangeRate as ExchangeRateExchangeRate;

class ExchangeRateUpdateController extends Controller
{
    public function __construct(private ExchangeRateExchangeRate $rateService)
    {
    }
    /**
     * Handle the incoming request.
     */
    public function __invoke(ExchangeRateUpdateRequest $request)
    {
        return $this->rateService->update($request);
    }
}
