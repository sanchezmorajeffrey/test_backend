<?php

namespace App\Http\Controllers\Api\ExchangeRate;

use App\Models\ExchangeRate;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Http\Resources\ExchangeRateCollection;
use App\Http\Requests\ExchangeRatePostRequest;
use App\Services\ExchangeRate\ExchangeRate as ExchangeRateExchangeRate;

class ExchangeRateController extends Controller
{
    public function __construct(private ExchangeRateExchangeRate $rateService)
    {
    }
    /**
     * Display a listing of the resource.
     */
    public function index(): ExchangeRateCollection
    {
        return $this->rateService->index();
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ExchangeRatePostRequest $request, ExchangeRate $rate): JsonResponse
    {
        return $this->rateService->store($request, $rate);
    }
}
