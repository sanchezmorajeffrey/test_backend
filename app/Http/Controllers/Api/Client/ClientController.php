<?php

namespace App\Http\Controllers\Api\Client;

use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Http\Resources\ClientResource;
use App\Services\Client\ClientService;
use App\Http\Resources\ClientCollection;
use App\Http\Requests\StoreclientRequest;
use App\Http\Requests\UpdateClientRequest;

class ClientController extends Controller
{
    public function __construct(private ClientService $clientService)
    {
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): ClientCollection
    {
        return $this->clientService->index($request);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreclientRequest $request, Client $client): JsonResponse
    {
        return $this->clientService->store($request->validated(), $client);
    }

    /**
     * Display the specified resource.
     */
    public function show(Client $client): ClientResource
    {
        return $this->clientService->show($client);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateClientRequest $request, Client $client): JsonResponse
    {
        return $this->clientService->update($request->validated(), $client);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Client $client): Response
    {
        return $this->clientService->destroy($client);
    }
}
