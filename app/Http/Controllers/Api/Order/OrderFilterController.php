<?php

namespace App\Http\Controllers\Api\Order;

use App\Http\Controllers\Controller;
use App\Services\Order\OrderService;
use App\Http\Requests\ListOrderByFilterRequest;

class OrderFilterController extends Controller
{
    public function __construct(private OrderService $orderService)
    {
    }
    /**
     * Handle the incoming request.
     */
    public function __invoke(ListOrderByFilterRequest $request)
    {
        return $this->orderService->filterClientOrder($request);
    }
}
