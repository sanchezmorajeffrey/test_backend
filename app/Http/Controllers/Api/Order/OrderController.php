<?php

namespace App\Http\Controllers\Api\Order;

use App\Models\Order;
use Illuminate\Http\Request;
use App\Services\Order\OrderService;
use App\Http\Controllers\Controller;
use App\Http\Resources\OrderCollection;
use App\Http\Requests\StoreOrderRequest;

class OrderController extends Controller
{
    public function __construct(private OrderService $orderService)
    {
    }
    /**
     * Display a listing of the resource.
     */
    public function index(): OrderCollection
    {
        return $this->orderService->index();
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreOrderRequest $request, Order $order)
    {
        return $this->orderService->store($request, $order);
    }
}
