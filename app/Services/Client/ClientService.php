<?php

namespace App\Services\Client;

use App\Models\Client;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use App\Http\Resources\ClientResource;
use App\Http\Resources\ClientCollection;
use Symfony\Component\HttpFoundation\Response as Status;

class ClientService
{
    public function index($request): ClientCollection
    {
        $client = Client::select('code', 'first_name', 'last_name', 'email', 'phone', 'address')
            ->when($request->has('first_name'), function ($query) use ($request) {
                $query->where('first_name', $request->get('first_name'));
            })
            ->when($request->has('code'), function ($query) use ($request) {
                $query->where('code', $request->get('code'));
            })
            ->get();
        return new ClientCollection($client);
    }

    public function show($client): ClientResource
    {
        $client = Client::select('code', 'first_name', 'last_name', 'email', 'phone', 'address')
            ->where('id', $client->id)
            ->first();
        return new ClientResource($client);
    }

    public function store(array $request, $client): JsonResponse
    {
        $client = $client->create($request);
        return (new ClientResource($client))->response()->setStatusCode(Status::HTTP_CREATED);
    }

    public function update(array $request, $client): JsonResponse
    {
        $client->update($request);
        return (new ClientResource($client))->response()->setStatusCode(Status::HTTP_OK);
    }

    public function destroy($category): Response
    {
        $category->delete();
        return response()->noContent();
    }
}
