<?php

namespace App\Services\Order;

use App\Http\Resources\OrderCollection;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Resources\OrderResource;
use Symfony\Component\HttpFoundation\Response as Status;

class OrderService
{
    private const TAX_RATE = 0.15;

    public function index(): OrderCollection
    {
        $orders = Order::select('id', 'taxes', 'subtotal', 'total', 'status', 'currency', 'code', 'created_at', 'client_id')
            ->with('client:id,code,first_name,last_name,phone,address,email')
            ->with('items:id,order_id,product_id,quantity,price')
            ->get();
        return new OrderCollection($orders);
    }

    public function store(Request $request, $order)
    {
        $items = $request->input('products');
        $subtotal = $this->subtotal($items);
        $taxes = $this->taxes($subtotal);
        $total = $this->total($taxes, $subtotal);
        $order_fields = array_merge($request->except('products'), [
            'taxes' => $taxes,
            'subtotal' => $subtotal,
            'total' => $total,
        ]);
        $order = $order->create($order_fields);
        $order->items()->createMany($items);
        $order->load('items', 'client');
        return (new OrderResource($order))->response()->setStatusCode(Status::HTTP_CREATED);
    }

    private function subtotal(array $array): float
    {
        return array_sum(array_map(function ($item) {
            return $item['price'] * $item['quantity'];
        }, $array));
    }

    private function taxes(float $subtotal): float
    {
        return  $subtotal * self::TAX_RATE;
    }

    private function total(float $taxes, float $subtotal): float
    {
        return $taxes + $subtotal;
    }

    public function filterClientOrder($request)
    {
        $orders = Order::select('id', 'taxes', 'subtotal', 'total', 'status', 'currency', 'code', 'created_at', 'client_id')
            ->when($request->has('code_order'), function ($query) use ($request) {
                $query->where('code', $request->get('code_order'));
            })
            ->when($request->has('code_client'), function ($query) use ($request) {
                $query->where('client_id', $request->get('code_client'));
            })
            ->with('client:id,code,first_name,last_name,phone,address,email')
            ->with('items:id,order_id,product_id,quantity,price')
            ->get();
        return new OrderCollection($orders);
    }
}
