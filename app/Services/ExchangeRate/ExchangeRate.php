<?php

namespace App\Services\ExchangeRate;

use Carbon\CarbonImmutable;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;
use App\Http\Resources\ExchangeRateCollection;
use App\Models\ExchangeRate as ModelsExchangeRate;
use Symfony\Component\HttpFoundation\Response as Status;

class ExchangeRate
{
    public function index(): ExchangeRateCollection
    {
        $rate = ModelsExchangeRate::select('date', 'buy')
            ->where('date', now()->format('Y-m-d'))
            ->get();
        return new ExchangeRateCollection($rate);
    }

    public function store(Request $request, $rate): JsonResponse
    {
        $rate_item = $this->setYearMonth($request->input('rate'));
        $rate->insert($rate_item->toArray());
        return response()->json(['message' => 'It has been successfully registered!'])->setStatusCode(Status::HTTP_CREATED);
    }

    public function update($request): JsonResponse
    {
        $rate = ModelsExchangeRate::where('date', $request->get('date'))->firstOrFail();
        $rate->update(['buy' => $request->get('buy')]);
        return response()->json(['message' => 'It has been successfully updated!']);
    }

    public function setYearMonth(array $array): Collection
    {
        return collect($array)->map(function ($item) {
            $date = CarbonImmutable::parse($item['date']);
            return [
                'buy' => $item['buy'],
                'date' => $item['date'],
                'year' => $date->year,
                'month' => $date->month,
                'created_at' => now()->format('Y-m-d H:i:s'),
                'updated_at' => now()->format('Y-m-d H:i:s'),
            ];
        });
    }

    public function filterByMonth($request): ExchangeRateCollection
    {
        $rate = ModelsExchangeRate::select('date', 'buy')
            ->when($request->has('month') && $request->has('year'), function ($query) use ($request) {
                $query->where(
                    [
                        ['month', $request->get('month')],
                        ['year', $request->get('year')]
                    ]
                );
            })
            ->get();
        return new ExchangeRateCollection($rate);
    }

    public function destroy($request): Response
    {
        $exchangeRate = ModelsExchangeRate::where('date', $request->get('date'))->firstOrFail();
        $exchangeRate->delete();
        return response()->noContent();
    }
}
