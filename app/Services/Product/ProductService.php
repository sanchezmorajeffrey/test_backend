<?php

namespace App\Services\Product;

use App\Models\Product;
use App\Models\ExchangeRate;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use App\Http\Resources\ProductResource;
use App\Http\Resources\ProductCollection;
use Symfony\Component\HttpFoundation\Response as Status;

class ProductService
{
    public function index()
    {
        $product = Product::select('name', 'description', 'is_active', 'sku', 'price')
            ->ActiveProduct()
            ->addSelect(
                ['rate' => ExchangeRate::select('buy')->where('date', now()->format('Y-m-d'))->limit(1)]
            )->get();
        return new ProductCollection($product);
    }

    public function store(array $request, $product): JsonResponse
    {
        $product = $product->create($request);
        return (new ProductResource($product))->response()->setStatusCode(Status::HTTP_CREATED);
    }

    public function update(array $request, $product): JsonResponse
    {
        $product->update($request);
        return (new ProductResource($product))->response()->setStatusCode(Status::HTTP_OK);
    }

    public function destroy($product): Response
    {
        $product->delete();
        return response()->noContent();
    }

    public function filterSkuDescription($request)
    {
        $product = Product::select('name', 'sku', 'description', 'price', 'is_active')
            ->when($request->has('sku'), function ($query) use ($request) {
                $query->where('sku', $request->get('sku'));
            })->when($request->has('description'), function ($query) use ($request) {
                $query->where('description', 'LIKE', '%' . $request->get('description') . '%');
            })
            ->get();
        return new ProductCollection($product);
    }
}
