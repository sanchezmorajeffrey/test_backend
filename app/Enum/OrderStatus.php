<?php

namespace App\Enum;

enum OrderStatus: int
{
    case Rejected = 0;
    case Completed = 1;
}
