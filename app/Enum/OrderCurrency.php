<?php

namespace App\Enum;

enum OrderCurrency: string
{
    case NIO = 'NIO';
    case USD = 'USD';
}
