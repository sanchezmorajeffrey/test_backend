<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\Order\OrderController;
use App\Http\Controllers\Api\Client\ClientController;
use App\Http\Controllers\Api\Product\ProductController;
use App\Http\Controllers\Api\Order\OrderFilterController;
use App\Http\Controllers\Api\ExchangeRate\ExchangeRateController;
use App\Http\Controllers\Api\ExchangeRate\ExchangeRateDeleteController;
use App\Http\Controllers\Api\ExchangeRate\ExchangeRateFilterMonthController;
use App\Http\Controllers\Api\ExchangeRate\ExchangeRateUpdateController;
use App\Http\Controllers\Api\Product\ProductFilterController as ProductProductFilterController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('client', ClientController::class);
Route::get('rate/filter', ExchangeRateFilterMonthController::class);
Route::delete('rate', ExchangeRateDeleteController::class);
Route::patch('rate', ExchangeRateUpdateController::class);
Route::apiResource('rate', ExchangeRateController::class)->only(['index', 'store']);
Route::get('product/filter', ProductProductFilterController::class);
Route::apiResource('product', ProductController::class)->only(['index', 'store', 'update', 'destroy']);
Route::get('order/filter', OrderFilterController::class);
Route::apiResource('order', OrderController::class)->only(['index', 'store']);
