## Nombre del proyecto
TEST BACKEND.

## Requisitos
Aquí se especifican los requisitos necesarios para poder utilizar el proyecto, como por ejemplo:

- PHP versión 8.2.
- Laravel versión 10.0 o superior.
- MySQL versión 5.7 o superior.
## Instalación
Para instalar el proyecto, seguir los siguientes pasos:

- Clonar el repositorio: git clone https://github.com/usuario/proyecto.git
- Acceder a la carpeta del proyecto: cd proyecto
- Instalar las dependencias: composer install
- Crear el archivo de configuración de entorno: cp .env.example .env
- Generar la clave de aplicación: php artisan key:generate.
- Configurar el archivo .env con los datos de la base de datos.
- Correr las migraciones: php artisan migrate.